### DESAFIO CONEXASAUDE
Candidato: Rodrigo Carlos P. S. Rodrigues

### Requerimentos:
-Java 1.8
-Banco de dados MySQL

### Tecnologias:
-Spring Boot
-API Restfull

### Observações:
1- O script(dump) roda automaticamente quando o projeto é executado.
2- O caminho do script: src/main/resources/db/migraton/V01__desafio.sql;
3- Não pode ter um SCHEMA criado com o nome: desafio. Caso tenha, o script não ira conseguir roda e dará um erro quando for executar o script;
4- Caso apresente o erro referente ao horário brasileiro de verão ao subir a aplicação, efetuar o seguinte comando no MySQL: SET GLOBAL time_zone = '+4:00'

API Rest (endpoints)
--------------------

-Efetuar login:
POST: http://localhost:8081/api/login
BODY:
{
  "usuario": "rodrigo@email.com",
  "senha": "senhamedico1"
}

-Efetuar o logout:
POST: http://localhost:8081/api/logout
BODY:
{
  "token": "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJyb2RyaWdvQGVtYWlsLmNvbSIsImV4cCI6MTYwMjcwODMwNX0.TRc0zkwdIQQ_ub1awpLo5F9xaOwOC-e89w5b5cutWavmLYhOcRMnU3FH8T46WGtw7MXgnHKkhvtkbWGEV1KIuA"
}

-Cadastrar paciente
POST: http://localhost:8081/paciente/cadastrar
BODY:
{
  "nome": "Rafael Braga",
  "cpf": "101.202.303-11",
  "idade": "33",
  "telefone": "(21) 3232-6565"
}

-Atualizar paciente
PUT: http://localhost:8081/paciente/atualizar/{id}
BODY:
{
  "nome": "Rafael Braga",
  "cpf": "101.202.303-11",
  "idade": "33",
  "telefone": "(21) 3232-6565"
}

-Deletar paciente
DELETE: http://localhost:8081/paciente/deletar/{id}

-Agendar paciente
POST: http://localhost:8081/medico/agendar
BODY:
{
  "data_hora_atendimento": "2020-08-03 09:00:00",
  "idpaciente": "2"
}


Pronto!