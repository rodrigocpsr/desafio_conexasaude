package br.com.conexasaude.desafio.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.conexasaude.desafio.model.Bloqueado;

public interface BlacklistRepository extends JpaRepository<Bloqueado, Integer> {

	Optional<Bloqueado> findByToken(String token);

}
