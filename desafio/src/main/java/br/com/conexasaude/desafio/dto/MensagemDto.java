package br.com.conexasaude.desafio.dto;

public class MensagemDto {
	
	private String mensagem;
	
	public MensagemDto() {
		super();
	}
	
	public MensagemDto(String mensagem) {
		super();
		this.mensagem = mensagem;
	}

	public String getMensagem() {
		return mensagem;
	}

	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}

}
