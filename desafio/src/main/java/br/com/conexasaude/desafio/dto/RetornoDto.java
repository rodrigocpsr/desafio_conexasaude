package br.com.conexasaude.desafio.dto;

import java.util.List;

import br.com.conexasaude.desafio.model.Agenda;

public class RetornoDto {	
	
	private String token;
	private String medico;
	private String especialidade;
	
	private List<AgendaDto> agendamento;

	public RetornoDto() {
		super();
	}

	public RetornoDto(String token, String medico, String especialidade, List<AgendaDto> agendamento) {
		super();
		this.token = token;
		this.medico = medico;
		this.especialidade = especialidade;
		this.agendamento = agendamento;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getMedico() {
		return medico;
	}

	public void setMedico(String medico) {
		this.medico = medico;
	}

	public String getEspecialidade() {
		return especialidade;
	}

	public void setEspecialidade(String especialidade) {
		this.especialidade = especialidade;
	}

	public List<AgendaDto> getAgendamento() {
		return agendamento;
	}

	public void setAgendamento(List<Agenda> agendamento) {
		this.agendamento = AgendaDto.converteDTO(agendamento);
	}
	
}
