package br.com.conexasaude.desafio.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.conexasaude.desafio.model.Paciente;

public interface PacienteRepository extends JpaRepository<Paciente, Integer> {
	
}
