package br.com.conexasaude.desafio.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.conexasaude.desafio.model.Usuario;

public interface LoginRepository extends JpaRepository<Usuario, Integer> {

	Optional<Usuario> findByUsuarioAndSenha(String usuario, String senha);

	Usuario findByUsuario(Object usuario);
	
}
