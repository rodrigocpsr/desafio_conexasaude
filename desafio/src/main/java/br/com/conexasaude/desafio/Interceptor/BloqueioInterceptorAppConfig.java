package br.com.conexasaude.desafio.Interceptor;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class BloqueioInterceptorAppConfig implements WebMvcConfigurer {
	@Autowired
	BloqueioInterceptor bloqueioInterceptor;

	@Override
	public void addInterceptors(InterceptorRegistry registry) {
		registry.addInterceptor(bloqueioInterceptor);
	}
}
