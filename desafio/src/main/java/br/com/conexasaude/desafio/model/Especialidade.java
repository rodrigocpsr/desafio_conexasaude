package br.com.conexasaude.desafio.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "ESPECIALIDADE")
public class Especialidade {
	
	@Id
	@Column(name = "IDESPECIALIDADE")
	private Integer idespecialidade;
		
	@Column(name = "NM_ESPECIALIDADE")
	private String nm_especialidade;

	public Especialidade() {
		super();
	}

	public Especialidade(Integer idespecialidade, String nm_especialidade) {
		super();
		this.idespecialidade = idespecialidade;
		this.nm_especialidade = nm_especialidade;
	}

	public Integer getIdespecialidade() {
		return idespecialidade;
	}

	public void setIdespecialidade(Integer idespecialidade) {
		this.idespecialidade = idespecialidade;
	}

	public String getNm_especialidade() {
		return nm_especialidade;
	}

	public void setNm_especialidade(String nm_especialidade) {
		this.nm_especialidade = nm_especialidade;
	}
	
}
