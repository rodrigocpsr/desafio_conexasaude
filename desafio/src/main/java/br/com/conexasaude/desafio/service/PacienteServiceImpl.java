package br.com.conexasaude.desafio.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.conexasaude.desafio.model.Paciente;
import br.com.conexasaude.desafio.repository.AgendaRepository;
import br.com.conexasaude.desafio.repository.PacienteRepository;

@Service
public class PacienteServiceImpl implements PacienteService {
	
	@Autowired
	PacienteRepository pacienteRepository;
	
	@Autowired
	AgendaRepository agendaRepository;

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void save(Paciente paciente) {
		pacienteRepository.save(paciente);
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public Optional<Paciente> buscarCadastroPaciente(Integer id) {
		Optional<Paciente> pacienteInfo = pacienteRepository.findById(id);
		return pacienteInfo;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void apagarCadastroPaciente(Integer id) {
		agendaRepository.apagarIdpaciente(id);
		pacienteRepository.deleteById(id);
	}	
	
}
