package br.com.conexasaude.desafio.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import br.com.conexasaude.desafio.dto.AgendaDto;
import br.com.conexasaude.desafio.service.AgendaService;

/**
 * Classe controladora de fluxos
 * @author rodrigo.rodrigues
 */
@RestController
public class MedicoController {
	
	@Autowired
	AgendaService agendaService;
	
	/**
	 * Método responsável por agendar um paciente
	 * @param agenda
	 * @return
	 */
	@PostMapping("/medico/agendar")
	public ResponseEntity<?> agendarPaciente(HttpServletRequest request, @RequestBody AgendaDto agendaDto) {
		try {
			agendaService.save(request, agendaDto);
			return new ResponseEntity<>(HttpStatus.CREATED);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
}
