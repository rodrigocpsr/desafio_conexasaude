package br.com.conexasaude.desafio.service;

import java.text.ParseException;

import javax.servlet.http.HttpServletRequest;

import br.com.conexasaude.desafio.dto.AgendaDto;

public interface AgendaService 
{
	/**
	 * Método responsavel por salvar o agendamento
	 * @param request 
	 * @param agenda
	 * @return
	 * @throws ParseException 
	 */
	void save(HttpServletRequest request, AgendaDto agendaDto) throws ParseException;
}
