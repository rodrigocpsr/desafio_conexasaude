package br.com.conexasaude.desafio.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import br.com.conexasaude.desafio.model.Paciente;
import br.com.conexasaude.desafio.service.PacienteService;

/**
 * Classe controladora de fluxos
 * @author rodrigo.rodrigues
 */
@RestController
public class PacienteController {
	
	@Autowired
	PacienteService pacienteService;
	
	/**
	 * Método responsavel por buscar o cadastro do paciente pelo id
	 * @param id
	 * @return
	 */
	@GetMapping("/paciente/{id}")
	public ResponseEntity<Paciente> buscarPaciente(@PathVariable("id") Integer id) {
		Optional<Paciente> pacienteInfo = pacienteService.buscarCadastroPaciente(id);

		if (pacienteInfo.isPresent()) {
			return new ResponseEntity<>(pacienteInfo.get(), HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}
	
	/**
	 * Método responsável por cadastrar o paciente
	 * @param paciente
	 * @return
	 */
	@PostMapping("/paciente/cadastrar")
	public ResponseEntity<?> cadastrarPaciente(@RequestBody Paciente paciente) throws Exception {
		try {
			pacienteService.save(paciente);
			return new ResponseEntity<>(HttpStatus.CREATED);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
	}
	
	/**
	 * Método responsável por editar o cadastro do paciente
	 * @param id
	 * @param paciente
	 * @return
	 */
	@PutMapping("/paciente/atualizar/{id}")
	public ResponseEntity<?> editarPaciente(@PathVariable("id") Integer id, @RequestBody Paciente paciente) throws Exception {
		try {
			Optional<Paciente> pacienteInfo = pacienteService.buscarCadastroPaciente(id);
			if (pacienteInfo.isPresent()) {
				Paciente _paciente = pacienteInfo.get();
				_paciente.setNome(paciente.getNome());
				_paciente.setCpf(paciente.getCpf());
				_paciente.setIdade(paciente.getIdade());
				_paciente.setTelefone(paciente.getTelefone());
				pacienteService.save(_paciente);
				return new ResponseEntity<>(HttpStatus.OK);
			} else {
				return new ResponseEntity<>(HttpStatus.NOT_FOUND);
			}
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
	}

	/**
	 * Método responsável por deletar o cadastro do paciente
	 * @param id
	 * @return
	 */
	@DeleteMapping("/paciente/deletar/{id}")
	public ResponseEntity<HttpStatus> deletarPaciente(@PathVariable("id") Integer id) {
		try {
			pacienteService.apagarCadastroPaciente(id);
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
	}
	
}
