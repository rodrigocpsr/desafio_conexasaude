package br.com.conexasaude.desafio.Interceptor;

import java.util.Optional;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import br.com.conexasaude.desafio.model.Bloqueado;
import br.com.conexasaude.desafio.repository.BlacklistRepository;

@Component
public class BloqueioInterceptor implements HandlerInterceptor {
	
	@Autowired
	BlacklistRepository blacklistRepository;

	static final String HEADER_STRING = "Authorization";
	
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
            throws Exception {
    	
    	String token = request.getHeader(HEADER_STRING);
    	
    	if (token != null) {
    		Optional<Bloqueado> blacklist = blacklistRepository.findByToken(token);
    		if (blacklist.isPresent()) {
    			response.setStatus(401);
    			return false;
    		}
    	}

        return true;
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler,
            ModelAndView modelAndView) throws Exception {
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler,
            Exception exception) throws Exception {
    }
    
}