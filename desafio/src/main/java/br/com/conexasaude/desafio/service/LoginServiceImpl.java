package br.com.conexasaude.desafio.service;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.xml.bind.ValidationException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.conexasaude.desafio.dto.RetornoDto;
import br.com.conexasaude.desafio.model.Agenda;
import br.com.conexasaude.desafio.model.Bloqueado;
import br.com.conexasaude.desafio.model.Usuario;
import br.com.conexasaude.desafio.repository.AgendaRepository;
import br.com.conexasaude.desafio.repository.BlacklistRepository;
import br.com.conexasaude.desafio.repository.LoginRepository;
import br.com.conexasaude.desafio.util.TokenUtil;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

@Service
public class LoginServiceImpl implements LoginService {
	
	private Logger logger = LoggerFactory.getLogger(LoginServiceImpl.class);
	
	@Autowired
	LoginRepository loginRepository;
	
	@Autowired
	AgendaRepository agendaRepository;
	
	@Autowired
	BlacklistRepository blacklistRepository;
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public RetornoDto autenticar(Usuario auth) throws Exception {
		
		RetornoDto retorno = new RetornoDto();
		
		try {
			Optional<Usuario> usuario = loginRepository.findByUsuarioAndSenha(auth.getUsuario(), auth.getSenha());
			Usuario _usuario = usuario.orElseThrow(() -> new ValidationException("Usuário e/ou senha inválido(s)"));
			retorno = MontarRetorno(_usuario);
		} catch (Exception e) {
			logger.error("Falha na tentativa de efetuar login", e);
			throw e;
		}
		
		return retorno;
	}

	/**
	 * Método responspavel por montar o retorno da autenticação
	 * @param _usuario
	 * @return
	 * @throws Exception
	 */
	private RetornoDto MontarRetorno(Usuario _usuario) throws Exception {
		
		RetornoDto retorno = new RetornoDto();
		
		try {
			String token = Jwts.builder().setSubject(_usuario.getUsuario())
					.setExpiration(new Date(System.currentTimeMillis() + TokenUtil.EXPIRATION_TIME))
					.signWith(SignatureAlgorithm.HS512, TokenUtil.SECRET).compact();
			
			retorno.setToken(token);
			retorno.setMedico(_usuario.getNome());
			retorno.setEspecialidade(_usuario.getEspecialidade().getNm_especialidade());
			
			List<Agenda> agendamento = agendaRepository.findByIdusuario(_usuario.getIdusuario());
			if (agendamento.size() > 0) {
				retorno.setAgendamento(agendamento);
			}
		} catch (Exception e) {
			logger.error("Falha na tentativa de montar o retorno da autenticação", e);
			throw e;
		}

		return retorno;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void invalidarToken(Bloqueado bloqueado) {	
		blacklistRepository.save(bloqueado);
	}

}
