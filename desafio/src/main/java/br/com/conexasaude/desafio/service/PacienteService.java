package br.com.conexasaude.desafio.service;

import java.util.Optional;

import br.com.conexasaude.desafio.model.Paciente;

public interface PacienteService 
{
	/**
	 * Método responsável por salvar o cadastro do paciente
	 * @param paciente
	 * @return
	 */
	void save(Paciente paciente);

	/**
	 * Método responsável por apagar o cadastro do paciente
	 * @param id
	 */
	void apagarCadastroPaciente(Integer id);

	/**
	 * Método responsável por buscar o cadastro de um paciente
	 * @param id
	 * @return
	 */
	Optional<Paciente> buscarCadastroPaciente(Integer id);
}
