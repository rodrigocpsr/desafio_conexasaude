package br.com.conexasaude.desafio.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import br.com.conexasaude.desafio.model.Agenda;

public interface AgendaRepository extends JpaRepository<Agenda, Integer> {
	
	List<Agenda> findByIdusuario(Integer idusuario);
	
    @Modifying
    @Transactional
	@Query(value = "DELETE FROM agenda WHERE idpaciente = ?", nativeQuery = true)
	public int apagarIdpaciente(Integer id);

    @Modifying
    @Transactional
	@Query(value = "INSERT INTO agenda VALUES (null, ?1, ?2, ?3)", nativeQuery = true)
	void salvarAgendamento(Integer id_paciente, Integer idusuario, String data);
}
