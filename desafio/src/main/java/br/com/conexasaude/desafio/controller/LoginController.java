package br.com.conexasaude.desafio.controller;

import javax.xml.bind.ValidationException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.conexasaude.desafio.dto.MensagemDto;
import br.com.conexasaude.desafio.dto.RetornoDto;
import br.com.conexasaude.desafio.model.Bloqueado;
import br.com.conexasaude.desafio.model.Usuario;
import br.com.conexasaude.desafio.service.LoginService;

/**
 * Classe controladora de fluxos
 * @author rodrigo.rodrigues
 */
@RestController
@RequestMapping("/api")
public class LoginController {
	
	private Logger logger = LoggerFactory.getLogger(LoginController.class);
	
	@Autowired
	LoginService loginService;
	
	/**
	 * Método responsável por fazer o login
	 * @param usuarioLogin
	 * @return RetornoDt
	 */
	@PostMapping("/login")
	public ResponseEntity<?> login(@RequestBody Usuario auth) throws Exception {	
		
		logger.info("----------------------------------");
		logger.info("LOGIN");
		logger.info("----------------------------------");
		logger.info(auth.getUsuario());

		try {
			RetornoDto retorno = loginService.autenticar(auth);
			return new ResponseEntity<RetornoDto>(retorno, HttpStatus.OK);
		} catch(ValidationException e) {
			MensagemDto retorno = new MensagemDto(e.getMessage());
			return new ResponseEntity<>(retorno, HttpStatus.UNAUTHORIZED);
		} finally {
			logger.info("----------------------------------");
		}
	}
	
	/**
	 * Método responsavel por fazer o logout
	 * @param auth
	 * @return
	 * @throws Exception
	 */
	@PostMapping("/logout")
	public ResponseEntity<?> logout(@RequestBody Bloqueado bloqueado) throws Exception {	
		
		logger.info("----------------------------------");
		logger.info("LOGOUT");
		logger.info("----------------------------------");
		
		try {
			loginService.invalidarToken(bloqueado); 
			return new ResponseEntity<>(HttpStatus.OK);
		} catch(Exception e) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		} finally {
			logger.info("----------------------------------");
		}
	}
	
}
