package br.com.conexasaude.desafio.service;

import br.com.conexasaude.desafio.dto.RetornoDto;
import br.com.conexasaude.desafio.model.Bloqueado;
import br.com.conexasaude.desafio.model.Usuario;

public interface LoginService {
	
	/**
	 * Método responsável por autenticar e montar o retorno
	 * @param user 
	 * @param usuarioLogin
	 * @return
	 */
	RetornoDto autenticar(Usuario auth) throws Exception;

	/**
	 * Método responsável por invalidar o token
	 * @param auth
	 * @return 
	 */
	void invalidarToken(Bloqueado bloqueado);

}
