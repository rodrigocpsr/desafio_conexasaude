package br.com.conexasaude.desafio.service;

import java.text.ParseException;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import br.com.conexasaude.desafio.dto.AgendaDto;
import br.com.conexasaude.desafio.model.Paciente;
import br.com.conexasaude.desafio.model.Usuario;
import br.com.conexasaude.desafio.repository.AgendaRepository;
import br.com.conexasaude.desafio.repository.LoginRepository;
import br.com.conexasaude.desafio.repository.PacienteRepository;

@Service
public class AgendaServiceImpl implements AgendaService {
	
	@Autowired
	AgendaRepository agendaRepository;
	
	@Autowired
	PacienteRepository pacienteRepository;
	
	@Autowired
	LoginRepository loginRepository;

	/**
	 * {@inheritDoc}
	 * @throws ParseException 
	 */
	@Override
	public void save(HttpServletRequest request, AgendaDto agendaDto) throws ParseException {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		Usuario usuario = loginRepository.findByUsuario(auth.getPrincipal());
		Paciente paciente = pacienteRepository.findById(agendaDto.getIdpaciente()).get();
		agendaRepository.salvarAgendamento(paciente.getId_paciente(),usuario.getIdusuario(),agendaDto.getData_hora_atendimento());
	}
	
}
