package br.com.conexasaude.desafio.dto;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import br.com.conexasaude.desafio.model.Agenda;

public class AgendaDto {
	
	private static final SimpleDateFormat SDF = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
	
	private Integer idpaciente;
	private String data_hora_atendimento;
	
	public AgendaDto() {
	}

	public AgendaDto(Agenda agenda) {
		this.idpaciente = agenda.getPaciente().getId_paciente();
		this.data_hora_atendimento = SDF.format(agenda.getData_hora_atendimento());
	}

	public Integer getIdpaciente() {
		return idpaciente;
	}

	public void setIdpaciente(Integer idpaciente) {
		this.idpaciente = idpaciente;
	}

	public String getData_hora_atendimento() {
		return data_hora_atendimento;
	}

	public void setData_hora_atendimento(String data_hora_atendimento) {
		this.data_hora_atendimento = data_hora_atendimento;
	}
	
	public static List<AgendaDto> converteDTO(List<Agenda> agendas){
		List<AgendaDto> agendaDto = new ArrayList<AgendaDto>();
		for (Agenda agenda : agendas) {
			agendaDto.add(new AgendaDto(agenda));
		}
		
		return agendaDto;
	}
	
}
