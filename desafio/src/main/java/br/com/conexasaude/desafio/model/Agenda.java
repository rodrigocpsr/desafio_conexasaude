package br.com.conexasaude.desafio.model;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "AGENDA")
public class Agenda {
	
	@Id
	@Column(name = "IDAGENDA")
    private Integer idagenda;
	
	@Column(name = "IDUSUARIO")
	private Integer idusuario;
	
	@Column(name = "DATA_HORA_ATENDIMENTO")
	private Date data_hora_atendimento;
	
	@OneToOne(cascade = CascadeType.ALL ,fetch = FetchType.EAGER)
	@JoinColumn(name = "IDPACIENTE")
	private Paciente paciente;

	public Agenda() {
		super();
	}

	public Agenda(Integer idagenda, Integer idusuario, Date data_hora_atendimento) {
		super();
		this.idagenda = idagenda;
		this.idusuario = idusuario;
		this.data_hora_atendimento = data_hora_atendimento;
	}

	public Integer getIdagenda() {
		return idagenda;
	}

	public void setIdagenda(Integer idagenda) {
		this.idagenda = idagenda;
	}

	public Integer getIdusuario() {
		return idusuario;
	}

	public void setIdusuario(Integer idusuario) {
		this.idusuario = idusuario;
	}

	public Date getData_hora_atendimento() {
		return data_hora_atendimento;
	}

	public void setData_hora_atendimento(Date data_hora_atendimento) {
		this.data_hora_atendimento = data_hora_atendimento;
	}

	public Paciente getPaciente() {
		return paciente;
	}

	public void setPaciente(Paciente paciente) {
		this.paciente = paciente;
	}

}
