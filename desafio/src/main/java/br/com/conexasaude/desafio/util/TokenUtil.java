package br.com.conexasaude.desafio.util;

public class TokenUtil
{	
	public static final long EXPIRATION_TIME = 860_000_000; //Tempo de expiração = 10 dias
	public static final String SECRET = "d17f2933b20a267cdd7cf675d6f7b855";
	public static final String TOKEN_PREFIX = "Bearer";
	public static final String HEADER_STRING = "Authorization";
}
