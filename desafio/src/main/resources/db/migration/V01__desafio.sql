CREATE TABLE especialidade (
	idespecialidade INT AUTO_INCREMENT,
	nm_especialidade VARCHAR(50),
	PRIMARY KEY (idespecialidade)
);

CREATE TABLE usuario (
	idusuario INT AUTO_INCREMENT,
	idespecialidade INT,
	usuario VARCHAR(50),
	senha VARCHAR(50),
    nome VARCHAR(50),
	PRIMARY KEY (idusuario),
	FOREIGN KEY (idespecialidade) REFERENCES especialidade (idespecialidade)
);

CREATE TABLE paciente (
	idpaciente INT AUTO_INCREMENT,
	nome VARCHAR(50),
	cpf varchar(30),
	idade INT,
	telefone VARCHAR(20),
	PRIMARY KEY (idpaciente)
);

CREATE TABLE agenda (
	idagenda INT AUTO_INCREMENT,
	idpaciente INT,
    idusuario INT,
	data_hora_atendimento DATE,
	PRIMARY KEY (idagenda),
	FOREIGN KEY (idpaciente) REFERENCES paciente (idpaciente)
);

CREATE TABLE bloqueado (
	id INT AUTO_INCREMENT,
	token VARCHAR(255),
	PRIMARY KEY (id)
);

INSERT INTO especialidade VALUES (null,'Cardiologista');
INSERT INTO especialidade VALUES (null,'Generalista');
INSERT INTO especialidade VALUES (null,'Ortopedista');

INSERT INTO usuario VALUES (null,1,'rodrigo@email.com','senhamedico1','Dr Rodrigo');
INSERT INTO usuario VALUES (null,2,'rafael@email.com','senhamedico2','Dr Rafael');
INSERT INTO usuario VALUES (null,3,'douglas@email.com','senhamedico3','Dr Douglas');

INSERT INTO paciente VALUES (null,'Fernando Souza','107.015.977-80',24 ,'(21) 97268-2121');
INSERT INTO paciente VALUES (null,'Paulo Rodrigues','103.834.477-80',55 ,'(22) 92332-2140');
INSERT INTO paciente VALUES (null,'Maria Santos','108.015.977-80',42 ,'(21) 97268-2121');

INSERT INTO agenda VALUES (null,1,1,'2020-12-03 10:00:00');
INSERT INTO agenda VALUES (null,1,1,'2020-11-08 17:30:00');
INSERT INTO agenda VALUES (null,1,1,'2020-10-23 12:00:00');
INSERT INTO agenda VALUES (null,2,2,'2020-10-11 10:00:00');
INSERT INTO agenda VALUES (null,2,2,'2020-11-04 14:00:00');